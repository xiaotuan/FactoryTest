package com.android.factorytest.ring;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Toast;

import com.android.factorytest.BaseActivity;
import com.android.factorytest.Log;
import com.android.factorytest.R;

/**
 * 扬声器测试
 */
public class SpeakerTest extends BaseActivity {

    public static final int FLAG_SHOW_SILENT_HINT = 1 << 7;

    private MediaPlayer mPlayer;
    private AudioManager mAudioManager;

    private String mMediaName;
    private int mCurrentMusicVolume;
    private int mMaxMusicVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_speaker_test);

        super.onCreate(savedInstanceState);

        mMediaName = getString(R.string.speaker_test_media_name);
        mPlayer = new MediaPlayer();
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mMaxMusicVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    }

    @Override
    protected void onResume() {
        super.onResume();
        playMusic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopPlayMusic();
    }

    private void playMusic() {
        boolean enabledPassBt = true;
        try {
            mCurrentMusicVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mMaxMusicVolume, FLAG_SHOW_SILENT_HINT);
            AssetManager am = getAssets();
            AssetFileDescriptor afd = am.openFd(mMediaName);
            mPlayer.reset();
            mPlayer.setDataSource(afd.getFileDescriptor(),
                    afd.getStartOffset(), afd.getLength());
            mPlayer.setLooping(true);
            mPlayer.prepare();
            mPlayer.start();
        } catch (Exception e) {
            Log.e(this, "playMusic=>error: ", e);
            Toast.makeText(this, getString(R.string.speaker_test_play_fail, mMediaName), Toast.LENGTH_SHORT).show();
            enabledPassBt = false;
        }
        setPassButtonEnabled(enabledPassBt);
    }

    public void stopPlayMusic() {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
            mPlayer.reset();
            mPlayer.release();
        }
        int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        if (current == mMaxMusicVolume) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentMusicVolume, FLAG_SHOW_SILENT_HINT);
        }
    }
}
