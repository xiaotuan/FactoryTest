package com.android.factorytest;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

public class TestWatermarkService extends Service {
    private WindowManager mWm;
    private TextView mFloatView;
    private boolean mIsSMT = false;
    private boolean mIsShowSMT = false;

    public static final String ACTION_ENTER_FM = "action_enter_fm";
    public static final String ACTION_EXIT_FM = "action_exit_fm";
    public static final String ACTION_CHECK = "action_check";

    public static void launcher(Context c){
        Intent runIntent = new Intent(c, TestWatermarkService.class);
        c.startService(runIntent);
    }

    public static void launcherShowFm(Context c){
        Intent runIntent = new Intent(c, TestWatermarkService.class);
        runIntent.putExtra("action", ACTION_ENTER_FM);
        c.startService(runIntent);
    }

    public static void launcherHileFm(Context c){
        Intent runIntent = new Intent(c, TestWatermarkService.class);
        runIntent.putExtra("action", ACTION_EXIT_FM);
        c.startService(runIntent);
    }

    public static void launcherCheck(Context c){
        Intent runIntent = new Intent(c, TestWatermarkService.class);
        runIntent.putExtra("action", ACTION_CHECK);
        c.startService(runIntent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mWm = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE));
        mIsSMT = isHasSMT();
        if(mIsSMT && !isHasFm()){
            showFloatView("MMI测试未通过");
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideFloatView();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        handleCommand(intent);

        return START_STICKY;
    }

    private void handleCommand(Intent intent){
        if(null == intent){
            return;
        }

        String action = intent.getStringExtra("action");
        if(ACTION_ENTER_FM.equals(action)){
            if(!mIsSMT){
                String infoStr = "SMT测试未通过";
                if(!isHasFm()){
                    infoStr += "MMI测试未通过";
                }
                showFloatView(infoStr);
            }
        }else if(ACTION_EXIT_FM.equals(action)){
            if(!mIsSMT){
                hideFloatView();
            }
        }else if(ACTION_CHECK.equals(action)){
            if(isHasFm()){
                hideFloatView();
            }
        }
    }

    private void showFloatView(String txt){
        if(null == mFloatView){
            WindowManager.LayoutParams layoutparams;
            layoutparams = new WindowManager.LayoutParams();
            layoutparams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
            layoutparams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
            layoutparams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            layoutparams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            layoutparams.format = PixelFormat.TRANSLUCENT;
            layoutparams.gravity = Gravity.TOP | Gravity.LEFT;
            DisplayMetrics dm = new DisplayMetrics();
            mWm.getDefaultDisplay().getMetrics(dm);
            layoutparams.x = 0;
            layoutparams.y = 0;

            mFloatView = new TextView(this);
            mFloatView.setBackgroundResource(0x00000000);
            mFloatView.setTextColor(0xffff0000);
            mFloatView.setTextSize(16);

            try{
                mWm.addView(mFloatView, layoutparams);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        mFloatView.setText(txt);
        mFloatView.setVisibility(View.VISIBLE);
    }

    private void hideFloatView(){
        if(mFloatView != null){
            mWm.removeView(mFloatView);
            mFloatView = null;
        }
    }

    private boolean isHasSMT() {
        android.os.IBinder binder = ServiceManager.getService("NvRAMAgent");
        NvRAMAgent agent = NvRAMAgent.Stub.asInterface(binder);
        byte[] buff = null;
        try{
            buff = agent.readFile(59);
        }catch(Exception e){
            e.printStackTrace();
        }

        try {
            if(buff[59] == 'P'){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean isHasFm(){
        return (SystemProperties.getInt("persist.sys.factorymode", 0x0) == 0x1);
    }
}
