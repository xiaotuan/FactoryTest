package com.android.factorytest.annex;

import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.AudioSystem;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.factorytest.BaseActivity;
import com.android.factorytest.Log;
import com.android.factorytest.R;

import java.io.File;
import java.io.IOException;

public class DualMicrophoneTest extends BaseActivity {

    public static final int FLAG_SHOW_SILENT_HINT = 1 << 7;

    private TextView mMicrophoneStateTv;
    private Button mMainMicrophoneBt;
    private Button mSubMicrophoneBt;
    private Button mDualMicrophoneBt;
    private Button mPlayOrRecordBt;
    private MediaPlayer mPlayer;
    private MediaRecorder mRecorder;
    private AudioManager mAudioManager;

    private boolean mIsPlaying;
    private MicrophoneState mMicrophoneState;
    private String mFileName;

    enum MicrophoneState {
        DUAL, MAIN, SUB
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_dual_microphone_test);

        super.onCreate(savedInstanceState);

        mPlayer = new MediaPlayer();
        mIsPlaying = false;
        mMicrophoneState = MicrophoneState.DUAL;
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "audiorecordtest.3gp";

        mMicrophoneStateTv = (TextView) findViewById(R.id.dual_microphone_status);
        mMainMicrophoneBt = (Button) findViewById(R.id.main_microphone);
        mSubMicrophoneBt = (Button) findViewById(R.id.sub_microphone);
        mDualMicrophoneBt = (Button) findViewById(R.id.dual_microphone);
        mPlayOrRecordBt = (Button) findViewById(R.id.record_or_play);

        mMainMicrophoneBt.setOnClickListener(mTestClickListener);
        mSubMicrophoneBt.setOnClickListener(mTestClickListener);
        mDualMicrophoneBt.setOnClickListener(mTestClickListener);
        mPlayOrRecordBt.setOnClickListener(mTestClickListener);

        setPassButtonEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateMicrophoneState();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopPlaying();
        stopRecording();
        mIsPlaying = false;
        mPlayOrRecordBt.setText(R.string.dual_microphone_record_title);
        AudioSystem.setParameters("SET_MIC_CHOOSE=0");
        mMicrophoneState = MicrophoneState.DUAL;
    }

    private void updateMicrophoneState() {
        switch (mMicrophoneState) {
            case DUAL:
                mMicrophoneStateTv.setText(getString(R.string.dual_microphone_test_state,
                        getString(R.string.dual_microphone_title)));
                break;

            case MAIN:
                mMicrophoneStateTv.setText(getString(R.string.dual_microphone_test_state,
                        getString(R.string.main_microphone_title)));
                break;

            case SUB:
                mMicrophoneStateTv.setText(getString(R.string.dual_microphone_test_state,
                        getString(R.string.sub_microphone_title)));
                break;
        }
    }

    private boolean startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            File file = new File(mFileName);
            if (!file.exists() || file.length() == 0) {
                Toast.makeText(this, getString(R.string.record_file_not_exists), Toast.LENGTH_SHORT).show();
            } else {
                mPlayer.setDataSource(mFileName);
                mPlayer.prepare();
                mPlayer.start();
                return true;
            }
        } catch (IOException e) {
            Log.e(this, "startPlaying:", e);
        }
        return false;
    }

    private void stopPlaying() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    private boolean startRecording() {
        mRecorder = new MediaRecorder();
        try {
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile(mFileName);

            mRecorder.prepare();
            mRecorder.start();
            return true;
        } catch (Exception e) {
            Log.e(this, "startRecording=>error: ", e);
            Toast.makeText(this, R.string.dual_microphone_test_record_fail, Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    private void stopRecording() {
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }
    }

    private View.OnClickListener mTestClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.dual_microphone:
                    mMicrophoneState = MicrophoneState.DUAL;
                    AudioSystem.setParameters("SET_MIC_CHOOSE=0");
                    updateMicrophoneState();
                    break;

                case R.id.main_microphone:
                    mMicrophoneState = MicrophoneState.MAIN;
                    AudioSystem.setParameters("SET_MIC_CHOOSE=1");
                    updateMicrophoneState();
                    break;

                case R.id.sub_microphone:
                    mMicrophoneState = MicrophoneState.SUB;
                    AudioSystem.setParameters("SET_MIC_CHOOSE=2");
                    updateMicrophoneState();
                    break;

                case R.id.record_or_play:
                    if (mIsPlaying) {
                        if (startRecording()) {
                            mPlayOrRecordBt.setText(R.string.dual_microphone_play_title);
                            mIsPlaying = false;
                        }
                    } else {
                        if (startPlaying()) {
                            mPlayOrRecordBt.setText(R.string.dual_microphone_record_title);
                            mIsPlaying = true;
                        }
                    }
                    break;
            }
        }
    };
}
