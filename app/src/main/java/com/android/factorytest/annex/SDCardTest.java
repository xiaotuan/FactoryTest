package com.android.factorytest.annex;

import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.text.TextUtils;
import android.widget.TextView;
import android.text.format.Formatter;

import com.android.factorytest.BaseActivity;
import com.android.factorytest.R;

/**
 * ＳDCard测试项
 */
public class SDCardTest extends BaseActivity {

    private TextView mSdcardStateTv;
    private TextView mTotalSizeTv;
    private TextView mFreeSizeTv;
    private StorageManager mStorageManager;

    private String mSdcardPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_sdcard_test);

        super.onCreate(savedInstanceState);

        mStorageManager = (StorageManager) getSystemService(STORAGE_SERVICE);

        mSdcardStateTv = (TextView) findViewById(R.id.sdcard_state);
        mTotalSizeTv = (TextView) findViewById(R.id.sdcard_total_size);
        mFreeSizeTv = (TextView) findViewById(R.id.sdcard_free_size);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSdcardPath = getSdcardPath();
        if (!TextUtils.isEmpty(mSdcardPath)) {
            mSdcardStateTv.setText(R.string.sdcard_insert);
            String totalSizeStr = getSdcardTotalSize();
            mTotalSizeTv.setText(totalSizeStr);
            String freeSizeStr = getSdcardFreeSize();
            mFreeSizeTv.setText(freeSizeStr);
            setPassButtonEnabled(true);
        } else {
            mSdcardStateTv.setText(R.string.sdcard_not_insert);
            mTotalSizeTv.setText(R.string.sdcard_unknown_state);
            mFreeSizeTv.setText(R.string.sdcard_unknown_state);
        }
    }

    private String getSdcardPath() {
        StorageVolume[] valumes = mStorageManager.getVolumeList();
        if (valumes != null) {
            StorageVolume volume = null;
            for (int i = 0; i < valumes.length; i++) {
                volume = valumes[i];
                if (volume.allowMassStorage() && !volume.isEmulated() && volume.isRemovable()) {
                    String state = mStorageManager.getVolumeState(volume.getPath());
                    if (state != null && !state.equals(Environment.MEDIA_UNMOUNTABLE) && !state.equals(Environment.MEDIA_NOFS)
                            && !state.equals(Environment.MEDIA_REMOVED) && !state.equals(Environment.MEDIA_BAD_REMOVAL)) {
                        return volume.getPath();
                    }
                }
            }
        }
        return null;
    }

    private String getSdcardTotalSize() {
        StatFs statfs = new StatFs(mSdcardPath);
        long blockCount = statfs.getBlockCount();
        long blockSize = statfs.getBlockSize();
        long totalSize = blockCount * blockSize;
        return Formatter.formatFileSize(this, totalSize);
    }

    private String getSdcardFreeSize() {
        StatFs statfs = new StatFs(mSdcardPath);
        long blockSize = statfs.getBlockSize();
        long availableBlocks = statfs.getAvailableBlocks();
        long freeSize = availableBlocks * blockSize;
        return Formatter.formatFileSize(this, freeSize);
    }
}
