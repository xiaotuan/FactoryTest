package com.android.factorytest.annex;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.factorytest.BaseActivity;
import com.android.factorytest.R;

public class LedTest extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    private RadioGroup mLedGroup;
    private RadioButton mTurnOffLedRb;
    private RadioButton mRedLedRb;
    private RadioButton mBlueLedRb;
    private RadioButton mGreenLedRb;
    private RadioButton mWhiteLedRb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_led_test);

        super.onCreate(savedInstanceState);

        mLedGroup = (RadioGroup) findViewById(R.id.led_test_radio_group);
        mTurnOffLedRb = (RadioButton) findViewById(R.id.turn_off_led);
        mRedLedRb = (RadioButton) findViewById(R.id.red_led);
        mBlueLedRb = (RadioButton) findViewById(R.id.blue_led);
        mGreenLedRb = (RadioButton) findViewById(R.id.green_led);
        mWhiteLedRb = (RadioButton) findViewById(R.id.white_led);

        mLedGroup.setOnCheckedChangeListener(this);

        setPassButtonEnabled(true);
        filterRadioButton();
        mTurnOffLedRb.setChecked(true);
        turnOffLed();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.turn_off_led:
                turnOffLed();
                break;

            case R.id.red_led:
                turnOnRedLed();
                break;

            case R.id.blue_led:
                turnOnBlueLed();
                break;

            case R.id.green_led:
                turnOnGreenLed();
                break;

            case R.id.white_led:
                turnOnWhiteLed();
                break;
        }
    }

    private void filterRadioButton() {
        Resources res = getResources();
        if (!res.getBoolean(R.bool.support_red_led)) {
            mRedLedRb.setVisibility(View.GONE);
        }
        if (!res.getBoolean(R.bool.support_blue_led)) {
            mBlueLedRb.setVisibility(View.GONE);
        }
        if (!res.getBoolean(R.bool.support_green_led)) {
            mGreenLedRb.setVisibility(View.GONE);
        }
        if (!res.getBoolean(R.bool.support_white_led)) {
            mWhiteLedRb.setVisibility(View.GONE);
        }
    }

    private void turnOffLed() {

    }

    private void turnOnRedLed() {

    }

    private void turnOnBlueLed() {

    }

    private void turnOnGreenLed() {

    }

    private void turnOnWhiteLed() {

    }
}
