package com.android.factorytest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * 工厂测试应用广播接收器，主要用于接收启动暗码
 */
public class FactoryTestReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.i(this, "onReceive=>action: " + action);
        if ("android.provider.Telephony.SECRET_CODE".equals(action)) {
            Uri uri = intent.getData();
            if (Uri.parse(("android_secret_code://" +
                    context.getResources().getString(R.string.factory_test_secret_code))).equals(uri)) {
                Intent factoryTest = new Intent(context, FactoryTest.class);
                context.startActivity(factoryTest);
            }
        } else if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            if (context.getResources().getBoolean(R.bool.enabled_fighting_test)) {
                TestWatermarkService.launcher(context);
            }
        }
    }
}
