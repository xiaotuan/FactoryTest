package com.android.factorytest.sensor;

import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.UserHandle;
import android.widget.TextView;

import com.android.factorytest.BaseActivity;
import com.android.factorytest.Log;
import com.android.factorytest.R;

public class FingerprintSensorTest extends BaseActivity {


    private TextView mFingerprintTipTv;
    private FingerprintManager mFingerprintManager = null;
    private CancellationSignal mEnrollmentCancel;

    private boolean mEnrolling;
    private byte[] mFingerprintToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_fingerprint_test);

        super.onCreate(savedInstanceState);

        mEnrolling = false;
        mFingerprintManager =  (FingerprintManager)getSystemService(FINGERPRINT_SERVICE);

        mFingerprintTipTv = (TextView) findViewById(R.id.fingerprint_test_tip);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startEnrollment();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelEnrollment();
    }

    private void startEnrollment() {
        mEnrollmentCancel = new CancellationSignal();
        mFingerprintToken = new byte[69];
        mFingerprintManager.enroll(mFingerprintToken, mEnrollmentCancel, 0, mEnrollmentCallback);
        mEnrolling = true;
    }

    private void cancelEnrollment() {
        if (mEnrolling) {
            mEnrollmentCancel.cancel();
            mEnrolling = false;
        }
    }

    private FingerprintManager.EnrollmentCallback mEnrollmentCallback = new FingerprintManager.EnrollmentCallback() {

        @Override
        public void onEnrollmentProgress(int remaining) {
            Log.d(FingerprintSensorTest.this, "onEnrollmentProgress=>remaining: " + remaining);
            mFingerprintTipTv.setText(R.string.fingerprint_test_pass_tip);
            setPassButtonEnabled(true);
        }

        @Override
        public void onEnrollmentHelp(int helpMsgId, CharSequence helpString) {
            Log.d(FingerprintSensorTest.this, "onEnrollmentHelp=>id: " + helpMsgId + " help: " + helpString);
        }

        @Override
        public void onEnrollmentError(int errMsgId, CharSequence errString) {
            Log.d(FingerprintSensorTest.this, "onEnrollmentError=>id: " + errMsgId + " error: " + errString);
        }

    };
}
