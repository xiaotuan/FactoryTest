package com.android.factorytest.system;

import android.os.Bundle;
import android.os.SystemProperties;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;
import android.widget.Toast;

import com.android.factorytest.BaseActivity;
import com.android.factorytest.R;

/**
 * 校准综测测试
 */
public class RfCaliTest extends BaseActivity {

    private TextView mGsmSerialTv;
    private TextView mCalibrationTv;
    private TextView mComprehensiveSurveyTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_rf_cali_test);

        super.onCreate(savedInstanceState);

        mGsmSerialTv = (TextView) findViewById(R.id.gsm_serial);
        mCalibrationTv = (TextView) findViewById(R.id.calibration);
        mComprehensiveSurveyTv = (TextView) findViewById(R.id.comprehensive_survey);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String serial = getGSMSerial();
        boolean isCalibrationPass = isCalibrationPass();
        boolean isComprehensiveSurverPass = isComprehensiveSurveyPass();

        int normalColor = getColor(R.color.text_view_text_color);
        int redColor = getColor(R.color.red);
        int greenColor = getColor(R.color.green);
        boolean enabledPass = true;

        String info = null;
        String title = getString(R.string.gsm_serial_test_title);
        SpannableStringBuilder builder = null;
        ForegroundColorSpan blueSpan = null;
        if (!TextUtils.isEmpty(serial)) {
            info = title + serial;
            builder = new SpannableStringBuilder(info);
            blueSpan = new ForegroundColorSpan(greenColor);
        } else {
            enabledPass = false;
            info = title + "null";
            builder = new SpannableStringBuilder(info);
            blueSpan = new ForegroundColorSpan(redColor);
        }
        builder.setSpan(blueSpan, title.length(), info.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mGsmSerialTv.setText(builder);

        title = getString(R.string.calibration_test_title);
        if (isCalibrationPass) {
            info = title + getString(R.string.pass);
            builder = new SpannableStringBuilder(info);
            blueSpan = new ForegroundColorSpan(greenColor);
        } else {
            enabledPass = false;
            info = title + getString(R.string.fail);
            builder = new SpannableStringBuilder(info);
            blueSpan = new ForegroundColorSpan(redColor);
        }
        builder.setSpan(blueSpan, title.length(), info.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mCalibrationTv.setText(builder);

        title = getString(R.string.comprehensive_surver_test_title);
        if (isComprehensiveSurverPass) {
            info = title + getString(R.string.pass);
            builder = new SpannableStringBuilder(info);
            blueSpan = new ForegroundColorSpan(greenColor);
        } else {
            enabledPass = false;
            info = title + getString(R.string.fail);
            builder = new SpannableStringBuilder(info);
            blueSpan = new ForegroundColorSpan(redColor);
        }
        builder.setSpan(blueSpan, title.length(), info.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mComprehensiveSurveyTv.setText(builder);

        setTestCompleted(true);
        if (enabledPass) {
            setPassButtonEnabled(true);
//            setTestPass(true);
        } else {
            setPassButtonEnabled(false);
//            setTestPass(false);
        }
//        if (isAutoTest()) {
//            if (isTestPass()) {
//                Toast.makeText(this, getString(R.string.auto_test_pass_tip, getAutoTestNextTestDelayedTime() / 1000), Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(this, getString(R.string.auto_test_fail_tip, getAutoTestNextTestDelayedTime() / 1000), Toast.LENGTH_SHORT).show();
//            }
//            doOnAutoTest();
//        }
    }

    /**
     * 获取GSM序列号
     * @return 返回GSM序列号
     */
    public String getGSMSerial() {
        return SystemProperties.get("gsm.serial");
    }

    /**
     * 判断设备是否校准过
     * 通过判断GSM序列号第60位和61位（从０位算起）的值是否是10来判断该设备是否校准过
     * @return 如果设备校准过，返回true；否则返回false
     */
    public boolean isCalibrationPass() {
        boolean isCalibration = false;
        String gsmSerial = getGSMSerial();
        if (!TextUtils.isEmpty(gsmSerial) && gsmSerial.length() >= 62) {
            if (gsmSerial.charAt(60) == '1' && gsmSerial.charAt(61) == '0') {
                isCalibration = true;
            }
        }
        return isCalibration;
    }

    /**
     * 判断设备是否综测过
     * 通过判断GSM序列号第6２位（从０位算起）的值是否是P来判断该设备是否综测过
     * @return 如果设备综测过，返回true；否则返回false
     */
    public boolean isComprehensiveSurveyPass() {
        boolean isComprehensiveSurvey = false;
        String gsmSerial = getGSMSerial();
        if (!TextUtils.isEmpty(gsmSerial) && gsmSerial.length() >= 63) {
            if (gsmSerial.charAt(62) == 'P') {
                isComprehensiveSurvey = true;
            }
        }
        return isComprehensiveSurvey;
    }
}
